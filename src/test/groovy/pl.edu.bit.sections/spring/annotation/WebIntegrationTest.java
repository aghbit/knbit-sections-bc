package pl.edu.bit.sections.spring.annotation;

import org.springframework.test.context.ActiveProfiles;
import pl.edu.bit.sections.core.config.ApplicationProfile;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)

@org.springframework.boot.test.WebIntegrationTest("server.port:0")
@ActiveProfiles(ApplicationProfile.TEST)
public @interface WebIntegrationTest {

}

