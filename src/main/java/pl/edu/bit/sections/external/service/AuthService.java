package pl.edu.bit.sections.external.service;

import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.edu.bit.sections.rest.exception.ForbiddenException;
import pl.edu.bit.sections.rest.exception.UnauthorizedException;
import pl.edu.bit.sections.rest.json.*;

import java.util.Optional;

@Data
@Component
@Log4j
public class AuthService {

    public static final String AUTH_HEADER = "knbit-aa-auth";

    @Value("${service.auth.users.url}")
    private String authUsersUrl;

    @Value("${service.auth.authorize.url}")
    private String authorizationUrl;

    @Value("${service.auth.tokenRequest.url}")
    private String tokenRequestUrl;

    @Value("${service.auth.manageSectionPermission}")
    private String manageSectionPermission;

    @Value("${service.auth.manageAllSectionsPermission}")
    private String manageAllSectionsPermission;

    @Value("${service.userId}")
    private String microserviceUserId;

    @Value("${service.password}")
    private String microservicePassword;

    @Value("${service.auth.manageSectionRole}")
    private String manageSectionRole;

    public Optional<String> authorizeAsSectionManager(String authToken) {
        return authorizeWithoutExceptions(manageSectionPermission, authToken);
    }

    public Optional<String> authorizeAsManagerOfAllSections(String authToken) throws
            UnauthorizedException, ForbiddenException {
        return authorizeWithExceptions(manageAllSectionsPermission, authToken);
    }

    public void assignSectionManagerRoleTo(String userId) {
        String roleAssignmentUrl = String.format("%s/%s/roles", authUsersUrl, userId);
        HttpEntity<RoleNameJson> entity = new HttpEntity<>(new RoleNameJson(manageSectionRole), createMicroserviceHeader());
        ResponseEntity<String> response = new RestTemplate().exchange(roleAssignmentUrl, HttpMethod.PUT, entity, String.class);
        response.toString();
    }

    public void revokeSectionManagerRoleFrom(String userId) {
        String roleRemovalUrl = String.format("%s/%s/roles/%s", authUsersUrl, userId, manageSectionRole);
        HttpEntity entity = new HttpEntity<>(createMicroserviceHeader());
        ResponseEntity<String> response = new RestTemplate().exchange(roleRemovalUrl, HttpMethod.DELETE, entity, String.class);
        response.toString();
    }

    private String authorize(String permission, String authToken) {
        HttpEntity<AuthPermissionJson> entity = new HttpEntity<>(new AuthPermissionJson(permission), createAuthHeader(authToken));
        ResponseEntity<UserIdJson> userIdJsonResponse = new RestTemplate()
                .exchange(authorizationUrl, HttpMethod.POST, entity, UserIdJson.class);
        return userIdJsonResponse.getBody().getUserId();
    }

    private Optional<String> authorizeWithExceptions(String permission, String authToken) throws
            UnauthorizedException, ForbiddenException {
        Optional<String> userId = Optional.empty();
        try {
            userId = Optional.ofNullable(authorize(permission, authToken));
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    throw new UnauthorizedException();
                case FORBIDDEN:
                    throw new ForbiddenException();
                default:
                    log.error("Request authorization failed for reason other than 401/403", e);
            }
        }
        return userId;
    }

    private Optional<String> authorizeWithoutExceptions(String permission, String authToken) {
        Optional<String> userId = Optional.empty();
        try {
            userId = Optional.ofNullable(authorize(permission, authToken));
        } catch (HttpClientErrorException e) {
            // ignore exception
        }
        return userId;
    }

    private HttpHeaders createMicroserviceHeader() {
        TokenJson tokenJson = new RestTemplate().postForObject(tokenRequestUrl, new TokenRequestJson(microserviceUserId,
                microservicePassword), TokenJson.class);
        return createAuthHeader(tokenJson.getToken());
    }

    private HttpHeaders createAuthHeader(String authToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(AUTH_HEADER, authToken);
        return headers;
    }
}
