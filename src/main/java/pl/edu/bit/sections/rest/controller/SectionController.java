package pl.edu.bit.sections.rest.controller;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.edu.bit.sections.core.db.SectionRepository;
import pl.edu.bit.sections.core.exception.InvalidUserIdException;
import pl.edu.bit.sections.core.exception.JoinRequestAlreadySentException;
import pl.edu.bit.sections.core.exception.UserAlreadyInSectionException;
import pl.edu.bit.sections.core.exception.UserNotInSectionException;
import pl.edu.bit.sections.core.section.Section;
import pl.edu.bit.sections.core.user.UserStatusInSection;
import pl.edu.bit.sections.external.service.AuthService;
import pl.edu.bit.sections.rest.exception.ForbiddenException;
import pl.edu.bit.sections.rest.exception.SectionNotFoundException;
import pl.edu.bit.sections.rest.exception.UnauthorizedException;
import pl.edu.bit.sections.rest.json.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
class SectionController {

    @Autowired
    SectionRepository sectionRepository;

    @Autowired
    AuthService authService;

    @RequestMapping(
            value = "/sections",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void createSection(@Valid @RequestBody SectionCreationRequestJson requestJson,
                              @NonNull @RequestHeader(AuthService.AUTH_HEADER) String authToken) throws
            UnauthorizedException, ForbiddenException {
        authService.authorizeAsManagerOfAllSections(authToken);
        sectionRepository.save(
                Section.create(
                        requestJson.getName(),
                        requestJson.getDescription(),
                        requestJson.getLogoResource(),
                        requestJson.getHexColor()
                )
        );
    }

    @RequestMapping(
            value = "/sections",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<SectionInfoJson> getAllSectionsInfo(@RequestParam(value = "userId", required = false) String userId) {
        return sectionRepository.findAll()
                .stream()
                .map(section ->
                        new SectionInfoJson(
                                section.getId().toString(),
                                section.getName(),
                                section.getDescription(),
                                section.getLogoResource(),
                                section.getHexColor(),
                                userId == null ? "NOT_LOGGED_IN" : section.getUserStatusInSection(userId).toString()
                        ))
                .collect(Collectors.toList());
    }

    @RequestMapping(
            value = "/sections/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public SectionInfoJson getSectionInfo(@PathVariable String id) throws SectionNotFoundException {
        Section section = findSectionOrThrowException(id);
        return new SectionInfoJson(
                section.getId().toString(),
                section.getName(),
                section.getDescription(),
                section.getLogoResource(),
                section.getHexColor(),
                null
        );
    }

    @RequestMapping(
            value = "/sections/{id}/candidates",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void requestToJoinSection(@PathVariable String id, @Valid @RequestBody UserIdJson userId) throws
            InvalidUserIdException, UserAlreadyInSectionException, JoinRequestAlreadySentException,
            SectionNotFoundException {
        Section section = findSectionOrThrowException(id);
        section.addCandidateToSection(sectionRepository.getDatastore(), userId.getUserId());
    }

    @RequestMapping(
            value = "/sections/{id}/candidates",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<UserInfoJson> getAllCandidates(@PathVariable String id) throws SectionNotFoundException {
        Section section = findSectionOrThrowException(id);
        return section.getAllCandidates()
                .stream()
                .map(abstractUser ->
                        new UserInfoJson(
                                abstractUser.getId().toString(),
                                abstractUser.isSectionManager()
                        ))
                .collect(Collectors.toList());
    }

    @RequestMapping(
            value = "/sections/{id}/members",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addUserToSection(@PathVariable String id, @Valid @RequestBody UserIdJson userId,
                                 @NonNull @RequestHeader(AuthService.AUTH_HEADER) String authToken) throws
            InvalidUserIdException, UserAlreadyInSectionException, SectionNotFoundException, UnauthorizedException, ForbiddenException {
        Section section = findSectionOrThrowException(id);
        authorize(section, authToken);
        section.addUserToSection(sectionRepository.getDatastore(), userId.getUserId());
    }

    @RequestMapping(
            value = "/sections/{id}/members/{userId}",
            method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeUserFromSection(@PathVariable String id, @PathVariable String userId,
                                      @NonNull @RequestHeader(AuthService.AUTH_HEADER) String authToken) throws
            InvalidUserIdException, UserNotInSectionException, SectionNotFoundException, UnauthorizedException, ForbiddenException {
        Section section = findSectionOrThrowException(id);
        authorize(section, authToken);
        boolean wasSectionManager = section.getUserStatusInSection(userId) == UserStatusInSection.SECTION_MANAGER;
        section.removeUserFromSection(sectionRepository.getDatastore(), userId);
        boolean isSectionManagerInAnotherSection = sectionRepository.findAll().stream()
                .anyMatch(section1 -> section1.getUserStatusInSection(userId) == UserStatusInSection.SECTION_MANAGER);
        if (wasSectionManager && !isSectionManagerInAnotherSection) {
            authService.revokeSectionManagerRoleFrom(userId);
        }
    }

    @RequestMapping(
            value = "/sections/{id}/members",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<UserInfoJson> getAllSectionMembers(@PathVariable String id) throws SectionNotFoundException {
        Section section = findSectionOrThrowException(id);
        return section.getAllMembers()
                .stream()
                .map(abstractUser ->
                        new UserInfoJson(
                                abstractUser.getId().toString(),
                                abstractUser.isSectionManager()
                        ))
                .collect(Collectors.toList());
    }

    @RequestMapping(
            value = "/sections/{id}/members/{userId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public UserStatusInSectionJson getUserStatusInSection(@PathVariable String id, @PathVariable String userId)
            throws SectionNotFoundException, InvalidUserIdException {
        Section section = findSectionOrThrowException(id);
        return new UserStatusInSectionJson(section.getUserStatusInSection(userId).toString());
    }

    @RequestMapping(
            value = "/sections/{id}/sectionManagers",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void promoteToSectionManager(@PathVariable String id, @Valid @RequestBody UserIdJson userId,
                                        @NonNull @RequestHeader(AuthService.AUTH_HEADER) String authToken) throws
            SectionNotFoundException, InvalidUserIdException, UserNotInSectionException, UnauthorizedException,
            ForbiddenException {
        Section section = findSectionOrThrowException(id);
        authorize(section, authToken);
        section.promoteToSectionManager(sectionRepository.getDatastore(), userId.getUserId());
        authService.assignSectionManagerRoleTo(userId.getUserId());
    }

    private void authorize(Section section, String authToken) throws UnauthorizedException, ForbiddenException {
        Optional<String> userId = authService.authorizeAsSectionManager(authToken);
        if (userId.isPresent() && section.getUserStatusInSection(userId.get()) == UserStatusInSection.SECTION_MANAGER) {
            return;
        }
        authService.authorizeAsManagerOfAllSections(authToken);
    }

    private Section findSectionOrThrowException(String sectionId) throws SectionNotFoundException {
        Optional<Section> section = sectionRepository.findById(sectionId);
        if (!section.isPresent()) {
            throw new SectionNotFoundException();
        }
        return section.get();
    }

}
