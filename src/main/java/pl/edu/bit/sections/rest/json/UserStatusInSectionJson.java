package pl.edu.bit.sections.rest.json;

import lombok.Value;

@Value
public class UserStatusInSectionJson {
    private final String status;
}
