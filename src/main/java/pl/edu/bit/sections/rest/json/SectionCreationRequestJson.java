package pl.edu.bit.sections.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class SectionCreationRequestJson {
    private final String name;
    private final String description;
    private final String logoResource;
    private final String hexColor;

    @JsonCreator
    public SectionCreationRequestJson(
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("logoResource") String logoResource,
            @JsonProperty("hexColor") String hexColor){
        this.name = name;
        this.description = description;
        this.logoResource = logoResource;
        this.hexColor = hexColor;
    }
}
