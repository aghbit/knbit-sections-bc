package pl.edu.bit.sections.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class RoleNameJson {

    private final String roleName;

    @JsonCreator
    public RoleNameJson(@JsonProperty("roleName") String roleName){
        this.roleName = roleName;
    }
}
