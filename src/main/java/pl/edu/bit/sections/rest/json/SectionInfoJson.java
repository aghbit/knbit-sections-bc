package pl.edu.bit.sections.rest.json;

import lombok.Value;

@Value
public class SectionInfoJson {
    private final String id;
    private final String name;
    private final String description;
    private final String logoResource;
    private final String hexColor;
    private final String userStatus;
}
