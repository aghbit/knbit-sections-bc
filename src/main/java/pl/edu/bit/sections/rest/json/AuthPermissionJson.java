package pl.edu.bit.sections.rest.json;

import lombok.Value;

@Value
public class AuthPermissionJson {
    private final String permission;
}
