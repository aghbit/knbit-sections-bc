package pl.edu.bit.sections.rest.json;

import lombok.Value;

@Value
public class UserInfoJson {
    private final String userId;
    private final boolean sectionManager;

}
