package pl.edu.bit.sections.rest.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.edu.bit.sections.rest.config.JsonException;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends Exception {
    private final String reason = "UNAUTHORIZED";
}
