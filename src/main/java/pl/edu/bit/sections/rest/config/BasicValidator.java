package pl.edu.bit.sections.rest.config;

import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

/**
 * This basic validator checks if fields of json request are correct and throws {@link InvalidMessageBodyException} otherwise.
 * Only fields of String type with public no-arg getter are checked.
 * Field is correct if is not null and it's String value is not empty.
 */
@Component
public class BasicValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }

    @SneakyThrows
    @Override
    public void validate(Object target, Errors errors) {
        if ( // any field is blank
                Arrays.asList(target.getClass().getMethods()).stream()
                        .filter(method -> method.getName().startsWith("get"))
                        .filter(method -> method.getParameterCount() == 0)
                        .filter(method -> method.getReturnType().equals(String.class))
                        .anyMatch(method -> {
                            try {
                                return StringUtils.isBlank((String) method.invoke(target));
                            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                                // above filters should prevent exception being thrown
                                throw new AssertionError();
                            }
                        })) {
            throw new InvalidMessageBodyException();
        }
    }
}

