package pl.edu.bit.sections.rest.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.edu.bit.sections.rest.config.JsonException;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.NOT_FOUND)
public class SectionNotFoundException extends Exception {
    private final String reason = "SECTION_WITH_GIVEN_ID_NOT_FOUND";
}

