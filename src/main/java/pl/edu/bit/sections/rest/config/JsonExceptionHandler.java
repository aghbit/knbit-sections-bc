package pl.edu.bit.sections.rest.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Optional;

/**
 * Configures json conversion of exceptions annotated with {@link JsonException}.
 * <p>
 * Handler will check for existence of {@code JsonException} annotation and,
 * if present, will convert to json using only declared public getters of the class.
 * <p>
 * This class is intended only for Java classes!
 */
@ControllerAdvice
public class JsonExceptionHandler {

    @ExceptionHandler
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public ResponseEntity<String> handle(Exception exception) throws Exception {
        Optional<JsonException> jsonExceptionAnnotation = Optional.ofNullable(exception.getClass().getAnnotation(JsonException.class));
        if (jsonExceptionAnnotation.isPresent()) {
            try {
                return ResponseEntity.status(jsonExceptionAnnotation.get().value()).body(ExceptionToJsonConverter.toJson(exception));
            } catch (JsonProcessingException e) {
                // if processing Exception to json is not successful, throw original exception and let Spring handle it
                throw exception;
            }
        }
        throw exception;
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    @Order(Ordered.LOWEST_PRECEDENCE)
    public ResponseEntity<String> handle(@SuppressWarnings("unused") HttpMessageNotReadableException exception) throws Exception {
        return handle(new InvalidMessageBodyException());
    }

}
