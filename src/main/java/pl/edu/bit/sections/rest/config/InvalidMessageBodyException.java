package pl.edu.bit.sections.rest.config;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.BAD_REQUEST)
public class InvalidMessageBodyException extends Exception {
    private final String reason = "INVALID_REQUEST_BODY";
}

