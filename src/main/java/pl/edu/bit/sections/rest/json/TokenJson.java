package pl.edu.bit.sections.rest.json;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class TokenJson {
    private final String token;

    @JsonCreator
    public TokenJson(@JsonProperty("token") String token){
        this.token = token;
    }

}
