package pl.edu.bit.sections.core.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.edu.bit.sections.core.db.SectionRepository;
import pl.edu.bit.sections.core.section.Section;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Data
@Component
public class ColorDataBaseSeeder {

    private final SectionRepository sectionRepository;

    @Autowired
    public ColorDataBaseSeeder(SectionRepository sectionRepository) {
        this.sectionRepository = sectionRepository;
    }

    @PostConstruct
    public void provide() {
        if (sectionRepository.findAll().stream().anyMatch(section -> section.getHexColor() == null)) { // initialize section colors
            findSectionAndSetColor("Infra", "#673728");
            findSectionAndSetColor("Grafika", "#191C4F");
            findSectionAndSetColor("Algo", "#672F7E");
            findSectionAndSetColor(".NET", "#83BE71");
            findSectionAndSetColor("Idea Factory", "#00AEEF");
            findSectionAndSetColor("AI", "#C24243");
            findSectionAndSetColor("Events", "#ED088F");
            findSectionAndSetColor("AGK", "#0A7AB9");
        }
    }

    private void findSectionAndSetColor(String name, String color) {
        Optional<Section> section = sectionRepository.findByName(name);
        if (section.isPresent()) {
            Section existingSection = section.get();
            existingSection.setHexColor(color);
            sectionRepository.save(existingSection);
        }
    }
}