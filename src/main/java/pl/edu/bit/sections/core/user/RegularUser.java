package pl.edu.bit.sections.core.user;

import java.util.UUID;

public class RegularUser extends AbstractUser {

    public RegularUser(UUID id) {
        super(id, false);
    }

    //mongodb purpose only
    public RegularUser() {
    }

}