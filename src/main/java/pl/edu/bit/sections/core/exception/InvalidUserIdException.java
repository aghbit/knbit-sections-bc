package pl.edu.bit.sections.core.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.edu.bit.sections.rest.config.JsonException;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.BAD_REQUEST)
public class InvalidUserIdException extends Exception {
    private final String reason = "INVALID_USER_ID";
}
