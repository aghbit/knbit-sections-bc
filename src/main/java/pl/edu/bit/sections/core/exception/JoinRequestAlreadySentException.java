package pl.edu.bit.sections.core.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.edu.bit.sections.rest.config.JsonException;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.BAD_REQUEST)
public class JoinRequestAlreadySentException extends Exception{
    private final String reason = "REQUEST_TO_JOIN_SECTION_ALREADY_SENT";
}
