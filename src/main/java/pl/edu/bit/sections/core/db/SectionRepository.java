package pl.edu.bit.sections.core.db;

import lombok.NonNull;
import org.mongodb.morphia.Datastore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pl.edu.bit.sections.core.section.Section;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class SectionRepository {

    @Autowired
    private Datastore ds;

    public Datastore getDatastore(){
        return ds;
    }

    public Optional<Section> findById(@NonNull String id){
        UUID sectionUUID;
        try {
            sectionUUID = UUID.fromString(id);
        } catch (IllegalArgumentException e){
            return Optional.empty();
        }
        return Optional.ofNullable(ds.find(Section.class).field("id").equal(sectionUUID).get());
    }

    public Optional<Section> findByName(@NonNull String name) {
        return Optional.ofNullable(ds.find(Section.class).field("name").equal(name).get());
    }

    public List<Section> findAll() {
        return ds.find(Section.class).asList();
    }

    public void save(@NonNull Section section) {
        ds.save(section);
    }

    public void deleteAll(){
        ds.delete(ds.find(Section.class));
    }

    }
