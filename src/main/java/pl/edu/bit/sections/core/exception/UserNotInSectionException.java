package pl.edu.bit.sections.core.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.edu.bit.sections.rest.config.JsonException;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.BAD_REQUEST)
public class UserNotInSectionException extends Exception {
    private final String reason = "NO_SUCH_USER_IN_SECTION";
}
