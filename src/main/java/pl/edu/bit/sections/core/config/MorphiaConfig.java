package pl.edu.bit.sections.core.config;

import com.google.common.collect.Lists;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import lombok.SneakyThrows;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.bit.sections.core.section.Section;
import pl.edu.bit.sections.core.user.AbstractUser;

import java.util.List;

@Configuration
public class MorphiaConfig {

    @Value("${bit.sections.mongo.host}")
    private String hostname;

    @Value("${bit.sections.mongo.port}")
    private int port;

    @Value("${bit.sections.mongo.db}")
    private String dbName;

    @Value("${bit.sections.mongo.user}")
    private String user;

    @Value("${bit.mongo.password}")
    private String password;

    @Bean
    @SneakyThrows
    public Datastore provideDatastore() {
        List<MongoCredential> credentials = Lists.newArrayList(MongoCredential.createCredential(
                user,
                dbName,
                password.toCharArray()));

        Datastore ds = new Morphia()
                .map(Section.class)
                .map(AbstractUser.class)
                .createDatastore(new MongoClient(new ServerAddress(hostname, port), credentials), dbName);

        ds.ensureIndexes(); //creates indexes from @Index annotations in your entities
        ds.ensureCaps(); //creates capped collections from @Entity

        return ds;
    }
}
