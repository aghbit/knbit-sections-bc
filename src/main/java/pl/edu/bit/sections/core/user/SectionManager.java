package pl.edu.bit.sections.core.user;

import java.util.UUID;

public class SectionManager extends AbstractUser {

    public SectionManager(UUID id) {
        super(id, true);
    }

    //mongodb purpose only
    public SectionManager() {
    }

}
