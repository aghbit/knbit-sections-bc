package pl.edu.bit.sections.core.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.edu.bit.sections.rest.config.JsonException;

@Value
@EqualsAndHashCode(callSuper = true)
@JsonException(HttpStatus.BAD_REQUEST)
public class UserAlreadyInSectionException extends Exception {
    private final String reason = "USER_ALREADY_IN_SECTION";
}
