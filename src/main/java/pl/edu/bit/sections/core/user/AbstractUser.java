package pl.edu.bit.sections.core.user;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.Optional;
import java.util.UUID;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Embedded
public abstract class AbstractUser {
    private final UUID id;
    private final boolean sectionManager;

    public static Optional<UUID> parseId(String id) {
        Optional<UUID> userUUID;
        try {
            userUUID = Optional.of(UUID.fromString(id));
        } catch (IllegalArgumentException e) {
            userUUID = Optional.empty();
        }
        return userUUID;
    }

    //mongodb purpose only
    AbstractUser(){
        id = null;
        sectionManager = false;
    }

}
