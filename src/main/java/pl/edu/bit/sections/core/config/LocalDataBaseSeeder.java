package pl.edu.bit.sections.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.edu.bit.sections.core.db.SectionRepository;
import pl.edu.bit.sections.core.section.Section;

import javax.annotation.PostConstruct;

@Component
@Profile(ApplicationProfile.LOCAL)
public class LocalDataBaseSeeder {

    @Autowired
    private SectionRepository sectionRepository;

    @PostConstruct
    public void provide() {
        sectionRepository.deleteAll();

        String desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s";
        String img = "http://placehold.it/350x200";

        Section section1 = Section.create("Pierwsza sekcja", desc, img, "#83BE71");
        Section section2 = Section.create("Druga sekcja", desc, img, "#83BE71");
        Section section3 = Section.create("Trzecia sekcja", desc, img, "#83BE71");
        Section section4 = Section.create("Czwarta sekcja", desc, img, "#83BE71");
        Section section5 = Section.create("Piąta sekcja", desc, img, "#83BE71");
        Section section6 = Section.create("Szósta sekcja", desc, img, "#83BE71");

        sectionRepository.save(section1);
        sectionRepository.save(section2);
        sectionRepository.save(section3);
        sectionRepository.save(section4);
        sectionRepository.save(section5);
        sectionRepository.save(section6);
    }
}