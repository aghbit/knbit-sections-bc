package pl.edu.bit.sections.core.config;

public interface ApplicationProfile {
    String DEVELOPMENT = "development";
    String PRODUCTION = "production";
    String LOCAL= "local";
    String TEST = "test";
}
