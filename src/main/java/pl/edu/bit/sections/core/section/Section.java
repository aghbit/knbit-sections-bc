package pl.edu.bit.sections.core.section;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import pl.edu.bit.sections.core.exception.InvalidUserIdException;
import pl.edu.bit.sections.core.exception.JoinRequestAlreadySentException;
import pl.edu.bit.sections.core.exception.UserAlreadyInSectionException;
import pl.edu.bit.sections.core.exception.UserNotInSectionException;
import pl.edu.bit.sections.core.user.AbstractUser;
import pl.edu.bit.sections.core.user.RegularUser;
import pl.edu.bit.sections.core.user.SectionManager;
import pl.edu.bit.sections.core.user.UserStatusInSection;

import java.util.*;

@Data
@EqualsAndHashCode(of = "id")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
public class Section {
    @Id
    private final UUID id;

    @Embedded
    private final Set<AbstractUser> members;

    @Embedded
    private final Set<AbstractUser> candidates;

    private final String name;

    private String description;

    private String logoResource;

    private String hexColor;

    public static Section create(String name, String description, String logoResource, String hexColor) {
        return new Section(
                UUID.randomUUID(),
                new HashSet<>(),
                new HashSet<>(),
                name,
                description,
                logoResource,
                hexColor
        );
    }

    public void addCandidateToSection(Datastore ds, String userId) throws InvalidUserIdException, JoinRequestAlreadySentException,
            UserAlreadyInSectionException {
        UUID userUUID = parseUserIdOrThrowException(userId);
        AbstractUser newCandidate = new RegularUser(userUUID);
        if (members.contains(newCandidate)) {
            throw new UserAlreadyInSectionException();
        }
        if (candidates.contains(newCandidate)) {
            throw new JoinRequestAlreadySentException();
        }
        candidates.add(newCandidate);
        ds.findAndModify(
                ds.find(Section.class).field("id").equal(id),
                ds.createUpdateOperations(Section.class).add("candidates", newCandidate));
    }

    public Collection<AbstractUser> getAllCandidates() {
        return Collections.unmodifiableCollection(candidates);
    }

    public void addUserToSection(Datastore ds, String userId) throws InvalidUserIdException, UserAlreadyInSectionException {
        UUID userUUID = parseUserIdOrThrowException(userId);
        AbstractUser candidateToAdd = new RegularUser(userUUID);
        if (members.contains(candidateToAdd) || members.contains(new SectionManager(userUUID))) {
            return; // do nothing, so the method is idempotent
        }
        candidates.remove(candidateToAdd);
        ds.findAndModify(
                ds.find(Section.class).field("id").equal(id),
                ds.createUpdateOperations(Section.class).removeAll("candidates", candidateToAdd));
        members.add(candidateToAdd);
        ds.findAndModify(
                ds.find(Section.class).field("id").equal(id),
                ds.createUpdateOperations(Section.class).add("members", candidateToAdd));
    }

    public void removeUserFromSection(Datastore ds, String userId) throws InvalidUserIdException, UserNotInSectionException {
        UUID userUUID = parseUserIdOrThrowException(userId);
        AbstractUser userToRemove = new RegularUser(userUUID);
        AbstractUser sectionManagerToRemove = new SectionManager(userUUID);
        if (members.contains(userToRemove)) {
            members.remove(userToRemove);
            ds.findAndModify(
                    ds.find(Section.class).field("id").equal(id),
                    ds.createUpdateOperations(Section.class).removeAll("members", userToRemove));
        } else if (members.contains(sectionManagerToRemove)) {
            members.remove(sectionManagerToRemove);
            ds.findAndModify(
                    ds.find(Section.class).field("id").equal(id),
                    ds.createUpdateOperations(Section.class).removeAll("members", sectionManagerToRemove));
        } else if (candidates.contains(userToRemove)) {
            candidates.remove(userToRemove);
            ds.findAndModify(
                    ds.find(Section.class).field("id").equal(id),
                    ds.createUpdateOperations(Section.class).removeAll("candidates", userToRemove));
        } else {
            throw new UserNotInSectionException();
        }
    }

    public void promoteToSectionManager(Datastore ds, String userId) throws InvalidUserIdException, UserNotInSectionException {
        UUID userUUID = parseUserIdOrThrowException(userId);
        AbstractUser memberBeforePromotion = new RegularUser(userUUID);
        AbstractUser memberAfterPromotion = new SectionManager(userUUID);
        if (!members.contains(memberBeforePromotion)) {
            if (members.contains(memberAfterPromotion)) {
                return; // already a section manager
            } else {
                throw new UserNotInSectionException();
            }
        }
        members.remove(memberBeforePromotion); // remove regular user
        members.add(memberAfterPromotion);
        ds.findAndModify(
                ds.find(Section.class).field("id").equal(id),
                ds.createUpdateOperations(Section.class).removeAll("members", memberBeforePromotion));
        ds.findAndModify(
                ds.find(Section.class).field("id").equal(id),
                ds.createUpdateOperations(Section.class).add("members", memberAfterPromotion));
    }

    public Collection<AbstractUser> getAllMembers() {
        return Collections.unmodifiableCollection(members);
    }

    public UserStatusInSection getUserStatusInSection(String userId) {
        UUID userUUID;
        try {
            userUUID = parseUserIdOrThrowException(userId);
        } catch (InvalidUserIdException e) {
            return UserStatusInSection.NOT_IN_SECTION;
        }
        AbstractUser regularUser = new RegularUser(userUUID);
        if (members.contains(regularUser)) {
            return UserStatusInSection.MEMBER;
        } else if (candidates.contains(regularUser)) {
            return UserStatusInSection.CANDIDATE;
        } else if (members.contains(new SectionManager(userUUID))) {
            return UserStatusInSection.SECTION_MANAGER;
        }
        return UserStatusInSection.NOT_IN_SECTION;
    }


    private UUID parseUserIdOrThrowException(String userId) throws InvalidUserIdException {
        Optional<UUID> userUUID = AbstractUser.parseId(userId);
        if (!userUUID.isPresent()) {
            throw new InvalidUserIdException();
        }
        return userUUID.get();
    }

    // mongoDB purpose only
    private Section() {
        id = null;
        members = new HashSet<>();
        candidates = new HashSet<>();
        name = null;
        description = null;
        logoResource = null;
        hexColor = null;
    }

}
