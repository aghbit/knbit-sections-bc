package pl.edu.bit.sections.core.user;

public enum UserStatusInSection {

    NOT_IN_SECTION,

    CANDIDATE,

    MEMBER,

    SECTION_MANAGER,
}
