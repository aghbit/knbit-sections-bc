KNBIT-SECTIONS
========

Requirements
------------
- JDK 8
- MongoDB

Docker
------

### Building image from remote master branch
`docker build -t <yourname>/knbit-sections --no-cache -f docker/Dockerfile .`

### Running within container
`docker run -d --name <mongo-cointainer-name> mongo:3.0.3` - it will create container with running MongoDB 3.0.3

`docker run -d -P --link <mongo-container-name>:mongodb <yourname>/knbit-sections:dev` - it will create container 
with notifications app linked to MongoDB